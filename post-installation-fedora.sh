#!/bin/bash

## Ce script automatise toutes les installations et la configuration après installation de la distributions.
## Le coeur du script est composé de deux partie la première installe qui paquet logiciel et la seconde installe des thèmes,
## des extensions et configurations à la fois communs et spécifiques des différents environnement de bureau.

# Création des différents choix pour la suite

echo -n "Que voulez-vous continuer l'execution du script? ( oui ou non ) "
read reponse

case "$reponse" in

oui )  
# Installation update & logiciel du dépôt de base + des dépôts supplémentaires

echo "Mise à jour de la distribution et installation des paquets."

sudo cp /etc/dnf/dnf.conf /etc/dnf/dnf.conf.old
sudo -- bash -c 'echo -e "fastestmirror=true" >> /etc/dnf/dnf.conf' 
sudo -- bash -c 'echo -e "max_parallel_downloads=10" >> /etc/dnf/dnf.conf'

    # Mise à niveau et mise à jour

    sudo dnf clean all
    sudo dnf --refresh upgrade -y
    sudo dnf install -y dnf-plugin-system-upgrade
    sudo dnf system-upgrade download --releasever=38
    sudo dnf system-upgrade reboot
    sudo dnf install rpmconf
    sudo rpmconf -a
    sudo dnf repoquery --unsatisfied
    sudo dnf repoquery --duplicates
    sudo dnf list extras
    sudo dnf remove $(sudo dnf repoquery --extras --exclude=kernel,kernel-\*)
    sudo dnf autoremove
    sudo dnf install symlinks
    sudo symlinks -r /usr | grep dangling
    sudo symlinks -r -d /usr
    sudo rpm --rebuilddb
    sudo dnf distro-sync
    sudo dnf distro-sync --allowerasing
    sudo fixfiles -B onboot

    # Installation des paquets du dépôt de la distribution 

    sudo dnf install -y firefox
    sudo dnf install -y thunderbird
    sudo dnf install -y lollypop
    sudo dnf install -y gparted
    sudo dnf install -y wipe
    sudo dnf install -y git
    sudo dnf install -y soundconverter
    sudo dnf install -y timeshift
    sudo dnf install -y deja-dup
    sudo dnf install -y ghostwriter
    sudo dnf install -y keepassxc
    sudo dnf install -y pdfarranger
    sudo dnf install -y skrooge
    sudo dnf install -y texlive-scheme-full
    sudo dnf install -y gtk-murrine-engine sassc
    sudo dnf install -y partclone
    sudo dnf install -y fdupes
    sudo dnf install -y gdisk
    sudo dnf install -y scrub
    sudo dnf install -y gnome-maps
    sudo dnf install -y ImageMagick
    sudo dnf install -y java-latest-openjdk
    sudo dnf install -y gutenprint
    sudo dnf install -y sane-airscan
    sudo dnf install -y sushi
    sudo dnf install -y sassc

    # Ajout du dépôt RPM Fusion
    
    sudo dnf install -y --nogpgcheck https://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm
    sudo dnf install -y --nogpgcheck https://download1.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm
    sudo dnf upgrade -y
    sudo dnf install -y rpmfusion-{free,nonfree}-release-tainted
    sudo dnf install -y rpmfusion-{free,nonfree}-appstream-data
    
    # Installation de codecs
    
    sudo dnf install -y gstreamer1-plugins-{base,good,bad-free,good-extras,bad-free-extras} gstreamer1-plugin-mpg123 gstreamer1-libav
    sudo dnf install -y lame\* --exclude=lame-devel
    sudo dnf group upgrade -y --with-optional Multimedia
    sudo dnf install -y gstreamer1-plugins-{bad-freeworld,ugly}
    sudo dnf install -y libdvdcss
        
    # Accélération vidéo
    
    sudo dnf swap mesa-va-drivers mesa-va-drivers-freeworld
    sudo dnf swap mesa-vdpau-drivers mesa-vdpau-drivers-freeworld
    
    # Fwupd
    
    sudo dnf install -y fwupd
    sudo fwupdmgr refresh
    sudo fwupdmgr update

    # Suppression des paquets logiciels inutiles
    
    sudo dnf autoremove -y gnome-weather* libreoffice* rhythmbox* qemu*

    # Ajout du dépôt Flatpak

    flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
    flatpak install flathub net.cozic.joplin_desktop
    flatpak install flathub com.spotify.Client
    flatpak install flathub org.onlyoffice.desktopeditors
    flatpak install flathub org.musicbrainz.Picard
    flatpak install flathub info.portfolio_performance.PortfolioPerformance

	# VSCodium pour linux
	
	sudo rpmkeys --import https://gitlab.com/paulcarroty/vscodium-deb-rpm-repo/-/raw/master/pub.gpg
	printf "[gitlab.com_paulcarroty_vscodium_repo]\nname=download.vscodium.com\nbaseurl=https://download.vscodium.com/rpms/\nenabled=1\ngpgcheck=1\nrepo_gpgcheck=1\ngpgkey=https://gitlab.com/paulcarroty/vscodium-deb-rpm-repo/-/raw/master/pub.gpg\nmetadata_expire=1h" | sudo tee -a /etc/yum.repos.d/vscodium.repo
	sudo dnf install -y codium

    # Install d'un client Dropbox
    
    sudo cp dropbox.repo /etc/yum.repos.d/dropbox.repo
    sudo dnf upgrade dnf
    sudo dnf install -y nautilus-dropbox

    # Ajout de dépôt Balena Etcher

    curl -1sLf \
   'https://dl.cloudsmith.io/public/balena/etcher/setup.rpm.sh' \
   | sudo -E bash
   sudo dnf install -y balena-etcher-electron

    # Alias bash personnalisé  

    cp bash_aliases.txt ~/.bash_aliases
    git clone https://framagit.org/G-Bas/fichier-de-configuration-linux.git
    cd fichier-de-configuration-linux
    bash config.sh
    cd ..

    # Nom ordinateur personnalisé

    echo -n "Choisissez un nom pour votre ordinateur"
    read nom_ordinateur
    sudo hostnamectl set-hostname "$nom_ordinateur"

    # Thème et icones
    for d in icons themes ; do mkdir -p ~/.local/share/"$d" && ln -s ~/.local/share/"$d" ~/."$d" ; done

    git clone https://github.com/vinceliuice/Qogir-theme.git
    cd Qogir-theme/
    ./install.sh -d ~/.local/share/themes -t {default,ubuntu,manjaro} -c {standard,dark} -i fedora --libadwaita --tweaks round
    cd ..
                                            
    git clone https://github.com/vinceliuice/Qogir-icon-theme.git
    cd Qogir-icon-theme/
    ./install.sh -d ~/.local/share/icons -t {default,ubuntu,manjaro} -c all
    cd ..

    git clone https://github.com/vinceliuice/Fluent-icon-theme.git
    cd Fluent-icon-theme/
    ./install.sh -d ~/.local/share/icons {standard,green,red,orange,yellow}
    cd ..

    git clone https://github.com/vinceliuice/Fluent-gtk-theme.git
    cd Fluent-gtk-theme/
    ./install.sh -d ~/.local/share/themes -s compact -t {default,green,red,orange,yellow} -c {dark,standard} -i fedora --libadwaita --tweaks round float
    cd ..
    
    git clone https://github.com/vinceliuice/Jasper-gtk-theme.git
	cd Jasper-gtk-theme/
    ./install.sh -d ~/.local/share/themes -c dark -t {red,orange,blue,green,yellow} -s compact --libadwaita --tweaks black
    cd ..
    
    git clone https://github.com/vinceliuice/Orchis-theme.git
	cd Orchis-theme/
        ./install.sh -d ~/.local/share/themes -c standard -t {default,red,orange,green,yellow} -s compact --libadwaita  --tweaks primary black --round 5px
        cd ..
        
        git clone https://github.com/vinceliuice/vimix-gtk-themes.git
        cd vimix-gtk-themes/
        ./install.sh -d ~/.local/share/themes -c {standard,dark} -t {doder,jade,ruby} -s compact --libadwaita --tweaks flat
        cd ..
        
        git clone https://github.com/vinceliuice/Tela-circle-icon-theme.git
	cd Tela-circle-icon-theme/
	./install.sh -d ~/.icons {blue,red,green,yellow,orange,nord}
	cd ..
	
        git clone https://github.com/EliverLara/Nordic-Polar.git
        mv Nordic-Polar ~/.local/share/themes/Nordic-Polar

        git clone https://github.com/EliverLara/Kripton.git
        mv Kripton ~/.local/share/themes/Kripton

        git clone https://github.com/EliverLara/Juno.git
        mv Juno ~/.local/share/themes/Juno
        
        wget -qO- https://git.io/papirus-icon-theme-install | DESTDIR="$HOME/.icons" sh
        
        sudo dnf install -y breeze-cursor-theme

        # Extensions Gnome et des extensions et thèmes.
        
        sudo dnf install -y gnome-tweaks
        sudo dnf install -y gnome-extensions-app

        firefox https://extensions.gnome.org/extension/19/user-themes/
        firefox https://extensions.gnome.org/extension/307/dash-to-dock/
        firefox https://extensions.gnome.org/extension/4372/shutdowntimer/
        firefox https://extensions.gnome.org/extension/750/openweather/
        firefox https://extensions.gnome.org/extension/4269/alphabetical-app-grid/
        firefox https://extensions.gnome.org/extension/1144/shortcuts/
        firefox https://extensions.gnome.org/extension/4548/tactile/
        firefox https://extensions.gnome.org/extension/3193/blur-my-shell/
        
    # Nettoyage de tous les dossiers

    for d in Fluent-icon-theme Fluent-gtk-theme Qogir-theme Qogir-icon-theme Jasper-gtk-theme vimix-gtk-themes Tela-circle-icon-theme Orchis-theme fichier-de-configuration-linux ; do wipe -frie -q $d ; done
    
    # Configuration de gnome et redémarrage de nautlius
        sudo gsettings set org.gnome.desktop.peripherals.touchpad click-method 'areas'
        sudo gsettings reset org.gnome.shell app-picker-layout
        
;;

non )
echo "Le script n'executera pas aucune installation."
;;

* )
echo "Faites un choix."
;;

esac
